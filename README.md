# maynar-springboot-test
Spring boot gae app

tutorial
==========
https://github.com/GoogleCloudPlatform/getting-started-java/tree/master/appengine-standard-java8/springboot-appengine-standard

Running locally
================
	mvn appengine:run
	mvn appengine:devserver

To use vist: http://localhost:8080/

Deploying
==============
	mvn appengine:deploy
	mvn appengine:update
	
